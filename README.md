# dinfo and dinfox

Two utilities. dinfo is a basic partition and raid scanner.

dinfox is the 'upgraded' version and reports more data. However, specifically with Dell systems some of that data won't be available without the srvadmin toolset installed
dinfox also does not care about partitions

## Example output


### dinfo

```Checking drives, please wait ... 
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Disk       |     Size | Type | Partitions |   Sraid |    Speed |     Link |  Rpm |                  Model |          Serial |               Details | Errors | Temp |
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
|   /dev/sda |     4 TB |  gpt |          1 |     md2 | 6.0 Gb/s | 3.0 Gb/s | 7200 | WDC WD4003FZEX-00Z4SA0 | WD-WCC5DXXXXXXX | Western Digital Black |     No |  28C |
|   /dev/sdb |     4 TB |  gpt |          1 |     md2 | 6.0 Gb/s | 3.0 Gb/s | 7200 | WDC WD4003FZEX-00Z4SA0 | WD-WCC5DXXXXXXX | Western Digital Black |     No |  38C |
|   /dev/sdc |    16 GB |  gpt |          6 |         |          |          |      |              [Unknown] |                 |                       | Check! |      |
|   /dev/sdd | 512.1 GB |  dos |          3 | md1,md0 | 6.0 Gb/s | 6.0 Gb/s |      |  TOSHIBA THNSNH512GCST |    73IS10XXXXXX |                       |     No |  33C |
|   /dev/sde | 512.1 GB |  dos |          3 | md1,md0 | 6.0 Gb/s | 6.0 Gb/s |      |  TOSHIBA THNSNH512GCST |    73IS10XXXXXX |                       |     No |  27C |
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------```

### dinfox

```Checking devices, please wait ... 
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   Device |   Size | Hw_raid | Sw_raid |     Type | Device_speed | Link_speed | Rpm |                     Model |             Serial |                                         Details | Form_factor | Power_on_cycles | Unsafe_shutdowns |  Errors | Temp | Controller | Status |  State |
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 /dev/sda | 239 GB |  RAID-1 |         |  HW_Raid |              |            |     | PERC H330 Mini (Embedded) |                    |                                                 |             |                 |                  |         |      |            |     Ok |        |
  bus-0-0 | 240 GB |  Member |         | SATA 3.2 |     6.0 Gb/s |   6.0 Gb/s | SSD |            SSDSC2KG240G8R | PHYG01760XXXXXXXXX | Dell Certified Intel S4x00/D3-S4x10 Series SSDs |  2.5 inches |              63 |               61 | Unknown |  27C |          0 |     Ok | Online |
  bus-0-1 | 240 GB |  Member |         | SATA 3.2 |     6.0 Gb/s |   6.0 Gb/s | SSD |            SSDSC2KG240G8R | PHYG01760XXXXXXXXX | Dell Certified Intel S4x00/D3-S4x10 Series SSDs |  2.5 inches |              63 |               61 | Unknown |  26C |          0 |     Ok | Online |
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
```
